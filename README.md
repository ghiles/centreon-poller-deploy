<h1>Centreon-poller-deploy</h1>


Ce script permet de:
   * Déclarer un nouveau poller dans centreon.
   * Générer la configuration de l'ENGINE du nouveau poller.
   * Générer la configuration du Module Broker.
   * Créer un nouveau host en supervision (poller lui même) et le rattacher au nouveau poller.
   * Export/reload la configuration du nouveau poller.

<h2>Usage: </h2>

__bash poller-autodeploy.sh <user admin> <password> <poller name> <poller ip/dnsname> <ip cbd central>__

   * user admin: Utilisateur clapi avec droits d'admin.
   * password: Password de l'utilisateur clapi.
   * poller name: Nom du nouveau poller à créer.
   * poller ip/dnsname: L'ip ou le nom de DNS du nouveau poller.
   * ip cbd central: L'ip du broker central(serveur centreon web) pour la connexion vers le broker.


__Attention:__

Sur la plateforme centrale, pour des raisons de cohérence de la réplication, utilisez plutôt les noms d'hôtes déclarés dans le fichier /etc/hosts. car les IPs des pollers sur deux sites répliqués ne sont pas les mêmes.